/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webcebsms.testdb;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.sql.*;


/**
 *
 * @author nkeyapo
 */
@WebServlet(name = "TestDbServlet", urlPatterns = {"/TestDbServlet"})
public class TestDbServlet extends HttpServlet {

    
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        //Variables de connection
        String user = "yankees";
        String pass = "yankees";
        
        String jdbcUrl= "jdbc:mysql://localhost:3306/cebsms?useSSL=false&serverTimezone=UTC";
        String driver = "com.mysql.jdbc.Driver";
        
        //Obtention de la connection a la base de donnees
        try {
            PrintWriter out = response.getWriter();
            out.println("Connection to: " +jdbcUrl);
            Class.forName(driver);
            
            Connection myConn = DriverManager.getConnection(jdbcUrl, user, pass);
            out.println("Connexion reussie");
            myConn.close();
            
        }
        catch (Exception exc) {
            exc.printStackTrace();
            throw new ServletException(exc);
        }
    }

}
