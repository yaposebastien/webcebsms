/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webcebsms.spring.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author nkeyapo
 */
@Entity
@Table(name="communaute")
public class Communaute {
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="idcomm")
    private int idcomm;
    
    @Column(name="nomcomm")
    private String nomcomm;
    
    @Column(name="telephone")
    private String telephone;
    
    @Column(name="email")
    private String email;
    
    @Column(name="description")
    private String description;
    
     @Column(name="motdepasse")
    private String motdepasse;
    //Constructor

    public Communaute() {
    }
    
    //Getters et Setters

    public int getIdcomm() {
        return idcomm;
    }

    public void setIdcomm(int idcomm) {
        this.idcomm = idcomm;
    }

    public String getNomcomm() {
        return nomcomm;
    }

    public void setNomcomm(String nomcomm) {
        this.nomcomm = nomcomm;
    }

    public String getTelephone() {
        return telephone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMotdepasse() {
        return motdepasse;
    }

    public void setMotdepasse(String motdepasse) {
        this.motdepasse = motdepasse;
    }

    @Override
    public String toString() {
        return "Communaute{" + "idcomm=" + idcomm + ", nomcomm=" + nomcomm 
                + ", telephone=" + telephone + ", email=" + email 
                + ", description=" + description + ", motdepasse=" + motdepasse 
                + '}';
    }

    
   

    
    
    
    
    
}
