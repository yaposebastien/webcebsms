/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webcebsms.spring.service;

import com.webcebsms.spring.entity.Communaute;
import java.util.List;

/**
 *
 * @author nkeyapo
 */
public interface CommunauteService {

    public List<Communaute> searchCommunautes(String theSearchName);
    
    public List<Communaute> getCommunautes();
    
    //Creation methode pour sauvegarder Communaute dans le formulaire
    public void saveCommunaute(Communaute theCommunaute);
    
    //Pour recuperer l'ID de la communaute
    public Communaute getCommunaute(int theId);

    public void deleteCommunaute(int theId);
    
}
