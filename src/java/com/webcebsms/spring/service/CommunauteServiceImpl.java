/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webcebsms.spring.service;

import com.webcebsms.spring.entity.Communaute;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.webcebsms.spring.dao.CommunauteDAO;

/**
 *
 * @author nkeyapo
 */
@Service
public class CommunauteServiceImpl implements CommunauteService {
    
    
    //Injection of Communaute DAO
    @Autowired
    private CommunauteDAO communauteDAO;
    
    
    @Override
    @Transactional
    public List<Communaute> getCommunautes() {
        
        return communauteDAO.getCommunautes() ;
    }

    //Ajout du code pour sauvegarder la communaute
    @Override
    @Transactional
    public void saveCommunaute(Communaute theCommunaute) {
       communauteDAO.saveCommunaute(theCommunaute);
    }
    
    //Ajout du code pour la mise a jour de la communaute
    @Override
    @Transactional
    public Communaute getCommunaute(int theId) {
        
        return communauteDAO.getCommunaute(theId);
    }
    
    //Ajout du code pour la suppression

    @Override
    @Transactional
    public void deleteCommunaute(int theId) {
         
       communauteDAO.deleteCommunaute(theId); 
        
    }
    
    //Ajout de la methode d'implementation de la recherche
    @Override
    @Transactional
    public List<Communaute> searchCommunautes(String theSearchName) {
        
        return communauteDAO.searchCommunautes(theSearchName);
    }
    
}
