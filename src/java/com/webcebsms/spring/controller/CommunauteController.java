/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webcebsms.spring.controller;

import com.webcebsms.spring.entity.Communaute;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import com.webcebsms.spring.dao.CommunauteDAO;
import com.webcebsms.spring.service.CommunauteService;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author nkeyapo
 */
@Controller
@RequestMapping("/communaute")
public class CommunauteController {
    
    //Injection de notre Service pour le Communaute
    @Autowired
    private CommunauteService communauteService;
    
    @GetMapping("/list")
    public String listCommunautes(Model theModel) {
        
        //Obtenir la liste des Customers de notre Service
        List<Communaute> theCommunautes = communauteService.getCommunautes();
        
        //Ajout de Customers au Model
        theModel.addAttribute("communautes", theCommunautes);
        
        return "list-communautes";
    }
    
    //Ajout pour le formulaire
    @GetMapping("/showFormForAdd")
    public String showFormForAdd(Model theModel) {
        
        //Ajout de l'objet Communaute
        Communaute theCommunaute = new Communaute();
        
        //Ajout de l'instance de l'objet au Model 
        theModel.addAttribute("communaute", theCommunaute);
        
        return "communaute-form";
    }
    
    /*
    Ajout du code du bouton Ajout Communaute, donner le nom donne action dans le 
    formulaire.
    Passe aussi le nom du model donne dans notre formulaire
    */
    @PostMapping("saveCommunaute")
    public String saveCommunaute(@ModelAttribute("communaute") Communaute theCommunaute) {
        
    //Sauvegarde de notre Communaute
    communauteService.saveCommunaute(theCommunaute);
    
        return "redirect:/communaute/list";
    }
    
    /*
    Portion de code pour la mise a jour d'une communaute
    */
    @GetMapping("/showFormForUpdate")
    public String showFormForUpdate(@RequestParam("communauteId") int theId,
                                    Model theModel) {
        
        //Recuperer notre communaute dans l'interface service
        Communaute theCommunaute = communauteService.getCommunaute(theId);
        
        //Remplissage du formualire avec les donnees de la communaute selectionnee
        theModel.addAttribute("communaute", theCommunaute);
        
        return "communaute-form";
    }
    
    /*
    Code pour supprimer une communaute
    */
    @GetMapping("/delete")
    public String deleteCommunaute(@RequestParam("communauteId") int theId) {
        
        //Suppression de la communaute
        communauteService.deleteCommunaute(theId);
        return "redirect:/communaute/list";
    }
    
    //Code pour rechercher une communaute
    @PostMapping("search")
    public String searchCommunaute(@RequestParam("theSearchName") 
            String theSearchName, Model theModel) {
        
        //Recherche de la communaute dans notre service
        List<Communaute> theCommunautes = communauteService.searchCommunautes(theSearchName);
        
        //Ajout des communautes dans le modele
        theModel.addAttribute("communautes", theCommunautes);
        
        return "list-communautes";
        
    }
    
}
