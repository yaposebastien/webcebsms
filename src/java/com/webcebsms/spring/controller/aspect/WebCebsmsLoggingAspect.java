/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webcebsms.spring.controller.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.jboss.logging.Logger;
import org.springframework.stereotype.Component;

/**
 *
 * @author nkeyapo
 */
@Aspect
@Component
public class WebCebsmsLoggingAspect {
    
    //Definir la reference de notre Logger
    private Logger myLogger = Logger.getLogger(getClass().getName());
    
    //Definir la declaration de Pointcut des differents packages de notre projet
    @Pointcut("execution(* com.webcebsms.spring.controller.*.*(..))")
    private void forControllerPackage() {}
    
    @Pointcut("execution(* com.webcebsms.spring.dao.*.*(..))")
    private void forDaoPackage() {}
    
     @Pointcut("execution(* com.webcebsms.spring.service.*.*(..))")
    private void forServicePackage() {}
    
    @Pointcut("forControllerPackage() || forDaoPackage() || forServicePackage() ")
    private void forAppFlow() {}
    
    //Ajout du @Before advice
    @Before("forAppFlow()")
    public void before(JoinPoint theJoinPoint) {
        
        //Affichage de la methode appelee
        String theMethod = theJoinPoint.getSignature().toShortString();
        myLogger.info("====>> in @Before: calling " +theMethod);
        
        //Obtenir les arguments de la methode appele
        Object[] args = theJoinPoint.getArgs();
        
        //Parcours de la liste des arguments
        for (Object tempArg : args) {
            myLogger.info("======>> argument: " + tempArg);
        }
    }
    //Ajout @AfterReturning advice
    @AfterReturning(
            pointcut="forAppFlow()",
            returning="theResult"
            )
    public void afterReturning(JoinPoint theJoinPoint, Object theResult) {
        
        //Afficher la methode retournee
         String theMethod = theJoinPoint.getSignature().toShortString();
        myLogger.info("====>> in @After: calling " +theMethod);
        
        //Afficher les donnees retournees
        myLogger.info("=======>> resultat: " +theResult);
    }
    
}
