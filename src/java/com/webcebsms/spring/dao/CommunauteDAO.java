/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webcebsms.spring.dao;

import com.webcebsms.spring.entity.Communaute;
import java.util.List;

/**
 *
 * @author nkeyapo
 */
public interface CommunauteDAO {

    public List<Communaute> searchCommunautes(String theSearchName);
    
    public List<Communaute> getCommunautes();

    public void saveCommunaute(Communaute theCommunaute);

    public Communaute getCommunaute(int theId);

    public void deleteCommunaute(int theId);
    
    
}
