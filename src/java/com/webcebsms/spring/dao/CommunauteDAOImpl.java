/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webcebsms.spring.dao;

import com.webcebsms.spring.entity.Communaute;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;
import org.hibernate.query.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author nkeyapo
 */
@Repository
public class CommunauteDAOImpl implements CommunauteDAO {
    
    //Injection d'une session pour interagir avec la base de donnees
    @Autowired
    private SessionFactory sessionFactory;
    
    
    @Override
    //@Transactional //Permet Spring de gerer la debut et la fin de la transaction
    public List<Communaute> getCommunautes() {
        
        //Obtenir la courante session
        Session currentSession = sessionFactory.getCurrentSession();
        
        //Creer une requete
        Query<Communaute> theQuery = 
                currentSession.createQuery("from Communaute order by nomcomm", Communaute.class);
        
        //Executer la requete et obtenir les resultats
        List<Communaute> communautes = theQuery.getResultList();
        
        //Retourner les resultats
        return communautes;
    }

    /*
    Pour le button ajout d'une nvelle communaute
    */
    @Override
    public void saveCommunaute(Communaute theCommunaute) {
        
        //Obtention de la session courante
        Session currentSession = sessionFactory.getCurrentSession();
        
        //Sauvegarde du Communaute et mise a jour d'une communaute
        currentSession.saveOrUpdate(theCommunaute);
        
    }
    
    //Code pour la modification d'une communaute
   @Override
   public Communaute getCommunaute(int theId) {
       
    //Recuperer la session courante
    Session currentSession = sessionFactory.getCurrentSession();
    
    //Recuperer la Communaute 
    Communaute theCommunaute = currentSession.get(Communaute.class, theId);
    
    return theCommunaute;
    
   }
   
   //Code pour la suppression d'une communaute

    @Override
    public void deleteCommunaute(int theId) {
        
        //Obtention de la courante session
        Session currentSession = sessionFactory.getCurrentSession();
        
        //Suppression de la communaute a partir de la cle primaire
        Query theQuery = currentSession.createQuery("delete from Communaute where idcomm=:communauteId");
        
        theQuery.setParameter("communauteId", theId);
        
        theQuery.executeUpdate();
    }
   
    //Code pour la recherche d'une communaute
    @Override
    public List<Communaute> searchCommunautes(String theSearchName) {
        
        //Obtention de la session courante
        Session currentSession = sessionFactory.getCurrentSession();
        
        Query theQuery = null;
        
        //Effectue la recherche si theSearchName est non vide
        if (theSearchName!= null && theSearchName.trim().length() > 0) {
            
            //Le critere de recherche est le nom de la communaute
            theQuery = currentSession.createQuery("from Communaute where lower(nomcomm) like :theName", Communaute.class);
            theQuery.setParameter("theName", "%" + theSearchName.toLowerCase() + "%" );
        
        }
        else {
            //Retourner la liste de toutes les communautes si le resultat est null
            theQuery = currentSession.createQuery("from Communaute", Communaute.class);
            
        }
        
        //Excecution de la requete et affichage des resultats
        List<Communaute> communautes = theQuery.getResultList();
        
        return communautes;
        
    }
}
