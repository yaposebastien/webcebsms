<%-- 
    Document   : list-customers
    Created on : Nov 18, 2018, 3:00:04 PM
    Author     : nkeyapo
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Envoyez des messages gratuits a votre communaute de base</title>
        <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css"  rel="stylesheet">
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <link href="${pageContext.request.contextPath}/resources/css/ie10-viewport-bug-workaround.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link rel="stylesheet"  href="${pageContext.request.contextPath}/resources/customedStyle.css"
          type="text/css" />
        <script src="${pageContext.request.contextPath}/resources/js/ie-emulation-modes-warning.js"></script>
    </head>
    
    
    <body>
        

         <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="list">Attecoube.net</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#">Home</a></li>
            <li><a href="#about">About</a></li>
            <li><a href="#contact">Contact</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li role="separator" class="divider"></li>
                <li class="dropdown-header">Nav header</li>
                <li><a href="#">Separated link</a></li>
                <li><a href="#">One more separated link</a></li>
              </ul>
            </li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li><a href="../navbar/">Profil</a></li>
            <li><a href="communaute/formulaire-inscription-communaute">Enregistrement</a></li>
            <li class="active"><a href="./">Deconnexion <span class="sr-only">(current)</span></a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <div class="container">

      <!-- Main component for a primary marketing message or call to action -->
      <div class="jumbotron">
        <h1>Navbar example</h1>
        <p>This example is a quick exercise to illustrate how the default, static and fixed to top navbar work. It includes the responsive CSS and HTML, so it also adapts to your viewport and device.</p>
        <p>To see the difference between static and fixed top navbars, just scroll.</p>
        <p>
          <a class="btn btn-lg btn-primary" onclick="window.location.href='showFormForAdd'; return false;" role="button">Add New Community &raquo;</a>
        </p>
      </div>
      
                  <!--  add a search box -->
                  <div>
                      <form:form action="search" method="POST">
                        Recherche CEB: <input type="text" name="theSearchName" />
                
                        <input type="submit" value="Search" class="add-button" />
                        <br>
                        <br>
                      </form:form>
                  </div>
            
      
      
      <!-- Table to display the liste of communautes  -->
      <div>
          <table class="table table-hover">
                    <tr>
                        <th>Nom</th>
                        <th>Telephone</th>
                        <th>Email</th>
                        <th>Description</th>
                        <th>Action</th>
                    </tr>
                    <!-- Boucle pour afficher Customers -->
                    <!-- Code pour afficher une Communaute a modifier -->
                    <c:forEach var="tempComm" items="${communautes}">
                        
                        <!-- Debut du code du formulaire de mise a jour -->
                        <c:url var="updateLink" value="/communaute/showFormForUpdate">
                            <c:param name="communauteId" value="${tempComm.idcomm}" />
                        </c:url>
                        
                        <!-- Debut du code du formulaire pour la suppression -->
                        <c:url var="deleteLink" value="/communaute/delete">
                            <c:param name="communauteId" value="${tempComm.idcomm}" />
                        </c:url>
                        <tr>
                            <td> ${tempComm.nomcomm}</td>
                            <td> ${tempComm.telephone}</td>
                            <td> ${tempComm.email}</td>
                            <td> ${tempComm.description}</td>
                            
                            <td> 
                                <a href="${updateLink}">Update</a>
                                |
                                <a href="${deleteLink}"
                                   onclick="if (!(confirm('Voulez vous supprimer cette communaute?'))) return false" >Delete</a>
                            </td>
                        </tr>
                        
                    </c:forEach>
                </table>
      </div> 

    </div> <!-- /container -->
            

        </div><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="../../dist/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
       
    </body>
</html>
