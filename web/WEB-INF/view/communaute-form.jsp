<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html>

<head>
	 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Envoyez des messages gratuits a votre communaute de base</title>
        <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css"  rel="stylesheet">
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <link href="${pageContext.request.contextPath}/resources/css/ie10-viewport-bug-workaround.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link rel="stylesheet"  href="${pageContext.request.contextPath}/resources/customedStyle.css"
          type="text/css" />
        <script src="${pageContext.request.contextPath}/resources/js/ie-emulation-modes-warning.js"></script>
        
</head>

<body>
	
	     <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="list">Attecoube.net</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#">Home</a></li>
            <li><a href="#about">About</a></li>
            <li><a href="#contact">Contact</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li role="separator" class="divider"></li>
                <li class="dropdown-header">Nav header</li>
                <li><a href="#">Separated link</a></li>
                <li><a href="#">One more separated link</a></li>
              </ul>
            </li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li><a href="../navbar/">Profil</a></li>
            <li><a href="communaute/formulaire-inscription-communaute">Enregistrement</a></li>
            <li class="active"><a href="./">Deconnexion <span class="sr-only">(current)</span></a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

             
<div class="col-md-12 col-md-offset-2">       
<form:form action="saveCommunaute" modelAttribute="communaute" method="POST"
                   cssClass="">
            <form:hidden path="idcomm" />
<div>
         <div class="form-group row">
		<div class="col-xs-4">
			<label>Nom communaute:</label>
			<form:input path="nomcomm" cssClass="form-control" />
                        <br>
                        <label>Telephone:</label>
			<form:input path="telephone" cssClass="form-control" />
                        <br>
                        <label>Email:</label>
			<form:input path="email" cssClass="form-control" />
                        <br>
                        <label>Description:</label>
                        <form:textarea path="description" cssClass="form-control" />
                        <br>
                        <label>Mot de passe::</label>
                        <form:password path="motdepasse" cssClass="form-control" />
                        <br>
                        <button type="submit" class="btn btn-default" value="Save">Save</button>
                </div>
                            
         </div>
                        
</div>
</form:form>      
</div>
                
             
             
        <div style="clear; both;"></div>
		
		<p>
			<a href="${pageContext.request.contextPath}/customer/list"></a>
		</p>
    </div>
		
	
		
	

</body>

</html>

